package api

type Format struct {
	ID         int
	Name       string
	CategoryID int
}

type Formats map[int]Format