# API README

The purpose of this document is to provide the following:
  * Domain objects for the Kaster application

To create a mysql database in Docker, apply the following commands while changing the desired password:
`
sudo docker run --name kaster-db -p 3306:3306 -e MYSQL_ROOT_PASSWORD=my-secret-password -d mysql

The following command will not work until a few minutes after the previous command is run

sudo docker exec -i kaster-db sh -c 'mysql -u root -pmy-secret-password' < ./mysql/create.sql
`