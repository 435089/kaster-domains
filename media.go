package api

type Media struct {
	ID              int
	FormatID        int
	SubscriptionID  int
	ExternalFeedURL string
}

type Medias map[int]Media