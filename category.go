package api

type Category struct {
	ID   int
	Name string
}

type Categories map[int]Category