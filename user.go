package api

type User struct {
	ID           int
	FirstName    string
	LastName     string
	Username     string
	Password     string
	EmailAddress string
}

type Users map[int]User