DROP DATABASE IF EXISTS kaster;
CREATE DATABASE IF NOT EXISTS kaster;
USE kaster;

CREATE TABLE user (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    first_name VARCHAR(50), 
    last_name VARCHAR(100), 
    username VARCHAR(150) NOT NULL, 
    user_password VARCHAR(200) NOT NULL, 
    email_address VARCHAR(200) NOT NULL
);

INSERT INTO user (first_name, last_name, username, user_password, email_address) VALUES (
    'Mark',
    'Ehresman',
    'mehresman',
    'Password!1',
    '435089@gmail.com'
);

CREATE TABLE category (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    category_name VARCHAR(50) NOT NULL
);

INSERT INTO category (category_name) VALUE ('Audio');
INSERT INTO category (category_name) VALUE ('Video');

CREATE TABLE formats (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    format_name VARCHAR(20) NOT NULL,
    category_id INT NOT NULL,
    FOREIGN KEY fk_category(category_id)
    REFERENCES category(id)
);

CREATE TABLE subscription (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    subscription_name VARCHAR(100) NOT NULL,
    update_frequency INT NOT NULL,
    default_playback_speed INT NULL,
    FOREIGN KEY fk_user(user_id)
    REFERENCES user(id)
    ON DELETE CASCADE
);

CREATE TABLE media (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    format_id INT NOT NULL,
    subscription_id INT NOT NULL,
    external_feed_url VARCHAR(250) NOT NULL,
    FOREIGN KEY fk_formats(format_id)
    REFERENCES formats(id)
    ON UPDATE CASCADE,
    FOREIGN KEY fk_subscription(subscription_id)
    REFERENCES subscription(id)
);

CREATE TABLE episode (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    episode_name VARCHAR(50) NOT NULL,
    episode_description VARCHAR(250),
    external_url VARCHAR(250) NOT NULL,
    external_id varchar(50) NOT NULL,
    external_episode_number VARCHAR(50) NOT NULL,
    duration INT NOT NULL,
    current_progress INT NOT NULL,
    media_id INT NOT NULL,
    FOREIGN KEY fk_media(media_id)
    REFERENCES media(id)
    ON DELETE CASCADE
);

DROP USER IF EXISTS 'kaster'@'localhost';
CREATE USER IF NOT EXISTS 'kaster'@'localhost' IDENTIFIED BY 'D8D86E20BDC38A22B95C65D150BDF1D41AAA626F810C61E6A525ED3EFF78A32B';
GRANT SELECT ON kaster.* TO 'kaster'@'localhost';

GRANT INSERT ON kaster.subscription TO 'kaster'@'localhost';
GRANT INSERT ON kaster.episode TO 'kaster'@'localhost';
GRANT INSERT ON kaster.media TO 'kaster'@'localhost';

GRANT UPDATE ON kaster.subscription TO 'kaster'@'localhost';
GRANT UPDATE ON kaster.episode TO 'kaster'@'localhost';
GRANT UPDATE ON kaster.media TO 'kaster'@'localhost';

GRANT DELETE ON kaster.subscription TO 'kaster'@'localhost';
GRANT DELETE ON kaster.episode TO 'kaster'@'localhost';
GRANT DELETE ON kaster.media TO 'kaster'@'localhost';
FLUSH PRIVILEGES;