package api

type Episode struct {
	ID                    int
	Name                  string
	Description           string
	ExternalURL           string
	ExternalID            string
	ExternalEpisodeNumber string
	Duration              int
	CurrentTime           int
	MediaID               int
}

type Episodes map[int]Episode