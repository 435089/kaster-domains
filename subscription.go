package api

type Subscription struct {
	ID                   int
	UserID               int
	Name                 string
	UpdateFrequency      int
	DefaultPlaybackSpeed float32
}

type Subscriptions map[int]Subscription